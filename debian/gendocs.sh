#!/bin/sh
# vim: set noexpandtab:
# This script generates the *.docs files for each individual dictionary.

set -e

if pwd | grep -e '.*\/debian$'
then
    echo "$0: you must run this script from the root of the freedict source."
    exit 127
fi

## variables
FILES='COPYING AUTHORS INSTALL ChangeLog README README.txt README.md'
LANGLIST=`find . -maxdepth 1 -name '???-???' -printf "%f "`


for lang in $LANGLIST
do
    echo 'Generating docs for '$lang
    docsfile=debian/dict-freedict-$lang.docs
    for f in $FILES
    do
        if [ -f $lang/$f ]; then
            if [ `stat -c %s "$lang/$f"` -eq 0 ]
            then
                echo "adding $lang/$f to $docsfile."
                echo "$lang/$f" > $docsfile
            fi
        else
            echo "Skipping $lang/$f, is empty."
        fi
    done
done
